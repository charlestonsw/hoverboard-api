<?php

/**
 * Class Hoverboard_Object
 *
 * The base class on which all Hoverboard objects are built.
 *
 * 
 */
abstract class Hoverboard_Object {

    public $hapi_version = '4.2.01';

    /**
     * Create me.
     *
     * All parameters should be passed as a named array.
     *
     * The key (name) must be a property defined by the class or the value will not be set.
     */
    public function __construct( $parameters = null ) {
        if ( $parameters !== null) {
            foreach ( (array) $parameters as $property => $value ) {
                if ( $property === 'hapi_version' ) { next; }
                if ( property_exists( $this , $property ) ) {
                    $this->$property = $value;
                }
            }
        }
    }

}
