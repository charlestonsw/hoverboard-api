=== Search Widget Plus ===
Plugin Name:  Search Widget Plus
Contributors: charlestonsw
Donate link: http://www.hoverboard.tools/product/search-widget=plus/
Tags: search widget, search
Requires at least: 3.8
Tested up to: 4.2.2
Stable tag: 4.2.00
License: GLPv2 or later

A premium add-on for the free Search Widget plugin for Wordpress.

== Description ==

Extend the Search Widget plugin for WordPress with premium features.

= Development Requests =

Let me know what features and functionality you would like added to the Search Widget.
I will do my best to get them into the next monthly update.

= Hoverboard =

This plugin is [Hoverboard](http://hoverboard.tools/) compatible.

**What is Hoverboard?**

Hoverboard is a system that creates a seamless way to add sibling premium add-on packs for free plugins.
It includes a code framework for developers that builds on the WordPress APIs and coding standards.
The framework enables rapid plugin development for both free and premium plugins.
Using the Hoverboard Framework to build plugins makes it easier to create premium add-on packs for free plugins.
This means developers can start generating income.
Income means being able to spend more time crafting and creating.
More time crafting and creating means better plugins and better support which in turn creates a longer "shelf life" for the plugins you love and use every day.
Longer shelf life of a good plugin benefits the WordPress community as a whole.

_Feed the coders.  Get on Hoverboard._

== Changelog ==

= 4.2.00 =

* Initial release.