<?php

if ( ! class_exists( 'Hoverboard_Factory' ) ) {

    /**
     * Class Hoverboard_Factory
     *
     * A singleton factory that handles setup and invocation of HAPI objects.
     */
    class Hoverboard_Factory {

        /**
         * The location of the shared HAPI install directory.
         * @since 4.2.0
         * @var string
         */
        public $hapi_dir = null;

        /**
         * Holds the singleton instance of THE Hoverboard_Factory object.
         * @since 4.2.0
         * @var Hoverboard_Factory
         */
        public static $instance = false;

        /**
         * Install the Hoverboard Factory on WordPress activation
         * of a HAPI-compatible plugin.
         * @since 4.2.0
         */
        public static function init() {
            static $instance = false;
            if ( ! self::$instance ) {
                self::$instance = new Hoverboard_Factory();

                self::$instance->install();
            }
        }

        /**
         * Install Hoverboard Factory in a common area.
         * @since 4.2.0
         */
        function install() {
            self:$instance->make_hapi_directory();
        }

        /**
         * Make the shared HAPI directory and set the hapi_dir property to point there.
         * @since 4.2.0
         */
        function make_hapi_directory(){

            if ( is_null( self::$hapi_dir ) ) {

                $upload_dir = wp_upload_dir('hapi');
                $error = $upload_dir['error'];

                if (empty($error)) {

                    self::$hapi_dir = $upload_dir['path'];

                } else {

                    self::$hapi_dir = plugin_dir_path( __FILE__ );

                }

            }

        }
    }

}

// Install Hoverboard Factory during activation of any plugin that uses HAPI
//
register_activation_hook( __FILE__ , array( 'Hoverboard_Factory' , 'init' ) );
